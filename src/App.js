import { BrowserRouter, Routes, Route } from 'react-router-dom';
import style from './index.scss'
import Header from "./components/header/Header";
import { useState, useEffect }  from "react";
import ListProducts from './components/listProducts/ListProduct';
import Cart from './components/cart/Cart';
// import Selected from './components/header/selected/Selected';
import ListSelected from './components/listSelected/ListSelected';
// import { Context } from './Context';


function App() {


//  кошик і все що з ни пов'язано

  const [cartArray,setCartArray]=useState(JSON.parse(localStorage.getItem('cart')) || [])

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cartArray))},[cartArray]);

    // const Add = (arg)=>{
    //   setCartArray (cartArray => {
    //     console.log()
    //     return [...cartArray, Object]
    //       })
    // }
//  Обране

  const [cartSelected, setCartSelected]=useState(JSON.parse(localStorage.getItem('cartSelected'))|| [])
    useEffect(()=>{
      localStorage.setItem('cartSelected', JSON.stringify(cartSelected))},[cartSelected]);
      // console.log(cartSelected)
  


  return (
    

    <BrowserRouter>
    
    <Header cartArray={cartArray.length}/>
      <main className={style.main}>
      <Routes>
        <Route path='/home' element={<ListProducts cartArray={cartArray} setCartArray={setCartArray} cartSelected={cartSelected} setCartSelected={setCartSelected}/>}/>
        <Route path='/selected' element={<ListSelected  cartSelected={cartSelected} setCartSelected={setCartSelected} />}/>
        <Route path='/cart' element={<Cart cartArray={cartArray} setCartArray={setCartArray} cartSelected={cartSelected}/>}/> 
      </Routes>
      </main>
    </BrowserRouter>
    

  );
}

export default App;
