import PropTypes from 'prop-types'
import { useEffect, useState } from 'react'
import style from './Product.module.scss'
import Button from '../buttons/Button'
import { ReactComponent as Star } from './star.svg';
import Modal from '../modals/Modal';


function Product (props){

// Створюємо обьєкт товару
const Object={
    id    : props.id,
    title : props.title,
    image : props.image,
    price : props.price
}


// Перемикач стану Modal

    const [openModal, setOpenModal] = useState(false)
    
    function ToggleModal(){
        openModal!==true ? setOpenModal(true):setOpenModal(false);
        return openModal
    }

// Перемикач Star, обране (зміна кольору зірки onClick)
    const [fill, setColor] = useState ('#000')


// Завантаження вірного коліру зірки
useEffect(()=>{
    // console.log(props.cartSelected)

    props.cartSelected.forEach(e => {
    if (e.id === Object.id) {
        setColor("#ff0")
    }});
})



    const SelectedAdd= ()=>{
        const mySelect = JSON.parse(localStorage.getItem('cartSelected'))
        props.setCartSelected (c=>[...mySelect, Object])
    }

    const onClickChangeStar = ()=>{
        function Caught (){
            if (props.cartSelected.length === 0) {
                return true;
              }
            for (const e of props.cartSelected) {
                if (e.id !== Object.id) {
                  return true;
                }else {
                    return false
                }
              }
        }
        if(Caught(Object) && fill==="#000"){
            SelectedAdd()
            setColor("#ff0")
        }else{
            setColor("#000")
            // console.log(props.setCartSelected)
            props.setCartSelected(props.cartSelected.filter(e => e.id !== Object.id));
        }
    }



    return(
        <li key={props.id} id={props.id}>
            <p className={style.title}>
                {props.title}
                <Star className={style.star} fill={fill} width='25'height='25' onClick={onClickChangeStar}/>
            </p>
            <img src={props.image} alt="image_of_product" ></img>
            <p>{props.price} $</p>
            <Button 
                onClick ={ToggleModal} 
                backgroundButton = {props.backgroundButton}
                text = {props.textButton}
            />
            { openModal && <Modal 
                backgroundButton={props.backgroundButton}
                header={props.header}
                text = {props.text}
                buttonFirstText="OK"
                actions={ToggleModal}
                Object = {Object}
                id = {props.index}
                cartArray = {props.cartArray}
                setCartArray = {props.setCartArray}
                ToggleModal = {ToggleModal}
                displayFirst = {props.displayFirst}
                displaySecond = {props.displaySecond}
            />}  

        </li>
    )
}


Product.propTypes = { 
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired
}
export default Product