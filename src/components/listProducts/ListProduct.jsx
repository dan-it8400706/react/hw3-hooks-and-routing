import Product from "../product/Product";
import { useState, useEffect } from "react";
import style from './ListProduct.module.scss'



function ListProducts (props){

    const [products, setProducts] = useState([])

    useEffect(()=>{
      const url='https://dummyjson.com/products?limit=10'
      fetch(url)
        .then(res => res.json())
  
        .then(data => {
        //   console.log(data)
          setProducts(data.products)
        });
      
    },[])
    // console.log(props.setCartSelected)

    return(
        <ul className={style.ul}>
            {products.map(p => <Product 
            key={p.title+p.index} 
            title={p.title} 
            id={p.id} 
            price={p.price} 
            image={p.images[0]} 
            backgroundButton='green'
            header="Додавання товару до кошика"
            text = "Ви бажаєте додати товар до кошику?"
            cartArray={props.cartArray} 
            setCartArray={props.setCartArray}
            cartSelected={props.cartSelected} 
            setCartSelected={props.setCartSelected}
            textButton='Add to cart' 
            />)}
            displayFirst = "block"
            displaySecond = "None"
        </ul>
    
    )
}

export default ListProducts