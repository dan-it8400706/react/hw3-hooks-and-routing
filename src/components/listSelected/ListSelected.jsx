import { Link } from "react-router-dom"
import Product from "../product/Product"
import style from '../listProducts/ListProduct.module.scss'
import { useEffect, useState } from "react"

function ListSelected(props){

    // console.log(props.cartSelected)
    console.log(props.setCartSelected)

    // const OnClick = () => {

    //     const cart = JSON.parse(localStorage.getItem('cart'))
    //     props.setCartArray (c => {
    //         if (cart.find((p) => p.id === Object.id)){
    //             return alert("This ptoduct in cart")

    //             } else{
    //                 // console.log("else")
    //                 return [...cart, Object]
    //             }
    //         })
    // }

    // const SelectedControl= ()=>{
    //     const mySelect = JSON.parse(localStorage.getItem('cartSelected'))
    //     props.setCartSelected (c=>{
    //         if (mySelect.find((p) => p.id === Object.id)){
    //                     console.log('this card did selected')
    //         }else {
    //             return [...mySelect, Object]
    //         }
    //     })
    //     }

const [mySelected, setMySelected]=useState(JSON.parse(localStorage.getItem('cartSelected'))|| [])
    // let mySelected = JSON.parse(localStorage.getItem('cartSelected'))
useEffect(()=>{
    setMySelected(JSON.parse(localStorage.getItem('cartSelected')))
    // localStorage.setItem('cartSelected', JSON.stringify('cartSelected'))
},[mySelected])

    return(
        <Link to="/selected">
            <ul className={style.ul}>
                {
                mySelected.map(p => 
                <Product
                key={p.title+p.index} 
                name={p.title} id={p.id} 
                price={p.price} 
                title={p.title}
                image={p.image} 
                textButton=''
                backgroundButton='white'
                cartSelected={props.cartSelected} 
                setCartSelected={props.setCartSelected} 
                display= "None"
                displaySecond = "block"
                />)}
            </ul>
        </Link>
                
    )
}
export default ListSelected