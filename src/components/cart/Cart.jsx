import { Link } from "react-router-dom"
import style from '../listProducts/ListProduct.module.scss'
import Product from "../product/Product"

function Cart (props){

let myCart = JSON.parse(localStorage.getItem('cart'))
// console.log(myCart)
// console.log(props.cartSelected)
    return(
        <Link to="/cart">
            <ul className={style.ul}>
                {myCart.map(p => 
                <Product
                key={p.title+p.index} 
                name={p.title} id={p.id} 
                price={p.price} 
                title={p.title}
                image={p.image} 
                header="Видалення товару з кошика"
                text = "Ви бажаєте видалити товар з кошика?"
                textButton='Delete from cart'
                cartArray={props.cartArray} 
                setCartArray={props.setCartArray} 
                cartSelected={props.cartSelected}
                backgroundButton = "green"
                displayFirst = "None"
                displaySecond = "block"
                />)}
            </ul>
        </Link>

    )
}
export default Cart