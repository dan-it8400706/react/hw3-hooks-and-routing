import './Button.module.scss'


function Button (props){

const style = {
    background:props.backgroundButton,
    color:props.color,
    display:props.display
}  

    return(
        <button 
        className='btn' 
        onClick={props.onClick} 
        style={style}
        >{props.text}</button>
    )
}

export default Button