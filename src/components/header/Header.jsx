import Basket from "../ cart/cart"
import style from './Header.module.scss'
import Selected from "./selected/Selected"
import Home from "./logo/Home"

function Header ({cartArray}){
    return(
        <header className={style.header}>
            <div className={style.wrapper}>
                <div className="logo">
                    <Home text="Main"/>
                </div>
                <div className={style.cart}> 
                    <Basket cartCounter={cartArray}/>
                    <Selected />
                </div>
            </div> 


        </header>
    )
}

export default Header